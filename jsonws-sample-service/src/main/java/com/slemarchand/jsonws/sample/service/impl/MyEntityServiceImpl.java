/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.slemarchand.jsonws.sample.service.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.permission.UserPermissionUtil;
import com.slemarchand.jsonws.sample.model.UserInfos;
import com.slemarchand.jsonws.sample.model.UserInfosUpdate;
import com.slemarchand.jsonws.sample.service.base.MyEntityServiceBaseImpl;

/**
 * The implementation of the my entity remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.slemarchand.jsonws.sample.service.MyEntityService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Sébastien Le Marchand
 * @see MyEntityServiceBaseImpl
 * @see com.slemarchand.jsonws.sample.service.MyEntityServiceUtil
 */
public class MyEntityServiceImpl extends MyEntityServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.slemarchand.jsonws.sample.service.MyEntityServiceUtil} to access the my entity remote service.
	 */
	
	public UserInfos getUserInfos() throws PortalException {

		long currentUserId = this.getGuestOrUserId();
		
		return myEntityLocalService.getUserInfos(currentUserId);
	}
	
	public UserInfos getUserInfos(String screenName) throws PortalException {
		
		User currentUser = this.getUser();
		
		long companyId = currentUser.getCompanyId();

		UserInfos userInfos = myEntityLocalService.getUserInfos(companyId, screenName);
		
		// Attention de toujours bien assurer la sécurité au niveau de chaque 
		// méthode du "remote" service.
		
		UserPermissionUtil.check(
				getPermissionChecker(), userInfos.getUserId(), ActionKeys.VIEW);
		
		return userInfos;
	}
	
	public void updateUserInfos(long userId, UserInfosUpdate userInfosUpdate) throws PortalException {
		
		
		// Attention de toujours bien assurer la sécurité au niveau de chaque 
		// méthode du "remote" service.
				
		UserPermissionUtil.check(
						getPermissionChecker(), userId, ActionKeys.UPDATE);
		

		myEntityLocalService.updateUserInfos(userId, userInfosUpdate);
	}
}