/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.slemarchand.jsonws.sample.service.impl;

import java.util.Calendar;
import java.util.List;

import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.service.AddressLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.slemarchand.jsonws.sample.model.UserInfos;
import com.slemarchand.jsonws.sample.model.UserInfosUpdate;
import com.slemarchand.jsonws.sample.service.base.MyEntityLocalServiceBaseImpl;

/**
 * The implementation of the my entity local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.slemarchand.jsonws.sample.service.MyEntityLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MyEntityLocalServiceBaseImpl
 * @see com.slemarchand.jsonws.sample.service.MyEntityLocalServiceUtil
 */
public class MyEntityLocalServiceImpl extends MyEntityLocalServiceBaseImpl {

	public UserInfos getUserInfos(long userId) throws PortalException {
		
		User user = this.userLocalService.getUser(userId);
		
		UserInfos userInfos = new UserInfos();
		
		userInfos.setUserId(userId);
		userInfos.setLogin(user.getScreenName());
		userInfos.setEmail(user.getEmailAddress());
		userInfos.setFirstName(user.getFirstName());
		userInfos.setLastName(user.getLastName());
		
		return userInfos;
	}
	
	public UserInfos getUserInfos(long companyId, String screenName) throws PortalException {
		
		User user = this.userLocalService.getUserByScreenName(companyId, screenName);
		
		UserInfos userInfos = getUserInfos(user.getUserId());
		
		return userInfos;
	}
	
	public void updateUserInfos(long userId, UserInfosUpdate userInfosUpdate) throws PortalException {
		
		String firstName = userInfosUpdate.getFirstName();
		
		String lastName = userInfosUpdate.getLastName();
		
		User user = this.userLocalService.getUser(userId);
		
		Contact contact = user.getContact();

		if(Validator.isNull(firstName)) {
			firstName = user.getFirstName();
		}
		
		if(Validator.isNull(lastName)) {
			lastName = user.getLastName();
		}
		
		Calendar birthdayCal = CalendarFactoryUtil.getCalendar();

		birthdayCal.setTime(contact.getBirthday());

		int birthdayMonth = birthdayCal.get(Calendar.MONTH);
		int birthdayDay = birthdayCal.get(Calendar.DAY_OF_MONTH);
		int birthdayYear = birthdayCal.get(Calendar.YEAR);

		long[] groupIds = null;
		long[] organizationIds = null;
		long[] roleIds = null;
		List<UserGroupRole> userGroupRoles = null;
		long[] userGroupIds = null;

		ServiceContext serviceContext = new ServiceContext();
		
		userLocalService.updateUser(
			user.getUserId(), StringPool.BLANK, StringPool.BLANK,
			StringPool.BLANK, false, user.getReminderQueryQuestion(),
			user.getReminderQueryAnswer(), user.getScreenName(), user.getEmailAddress(),
			user.getFacebookId(), user.getOpenId(), true, null, user.getLanguageId(),
			user.getTimeZoneId(), user.getGreeting(), user.getComments(),
			firstName, user.getMiddleName(), lastName, contact.getPrefixId(),
			contact.getSuffixId(), user.getMale(), birthdayMonth, birthdayDay,
			birthdayYear, contact.getSmsSn(), contact.getFacebookSn(),
			contact.getJabberSn(), contact.getSkypeSn(), contact.getTwitterSn(),
			contact.getJobTitle(), groupIds, organizationIds, roleIds,
			userGroupRoles, userGroupIds, serviceContext);
	}
	
	@Reference
	private AddressLocalService addressLocalService;
}