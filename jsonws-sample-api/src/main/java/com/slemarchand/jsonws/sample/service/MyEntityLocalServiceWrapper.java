/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.slemarchand.jsonws.sample.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MyEntityLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see MyEntityLocalService
 * @generated
 */
@ProviderType
public class MyEntityLocalServiceWrapper implements MyEntityLocalService,
	ServiceWrapper<MyEntityLocalService> {
	public MyEntityLocalServiceWrapper(
		MyEntityLocalService myEntityLocalService) {
		_myEntityLocalService = myEntityLocalService;
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public String getOSGiServiceIdentifier() {
		return _myEntityLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.slemarchand.jsonws.sample.model.UserInfos getUserInfos(
		long userId) throws com.liferay.portal.kernel.exception.PortalException {
		return _myEntityLocalService.getUserInfos(userId);
	}

	@Override
	public com.slemarchand.jsonws.sample.model.UserInfos getUserInfos(
		long companyId, String screenName)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _myEntityLocalService.getUserInfos(companyId, screenName);
	}

	@Override
	public void updateUserInfos(long userId,
		com.slemarchand.jsonws.sample.model.UserInfosUpdate userInfosUpdate)
		throws com.liferay.portal.kernel.exception.PortalException {
		_myEntityLocalService.updateUserInfos(userId, userInfosUpdate);
	}

	@Override
	public MyEntityLocalService getWrappedService() {
		return _myEntityLocalService;
	}

	@Override
	public void setWrappedService(MyEntityLocalService myEntityLocalService) {
		_myEntityLocalService = myEntityLocalService;
	}

	private MyEntityLocalService _myEntityLocalService;
}