/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.slemarchand.jsonws.sample.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for MyEntity. This utility wraps
 * {@link com.slemarchand.jsonws.sample.service.impl.MyEntityLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see MyEntityLocalService
 * @see com.slemarchand.jsonws.sample.service.base.MyEntityLocalServiceBaseImpl
 * @see com.slemarchand.jsonws.sample.service.impl.MyEntityLocalServiceImpl
 * @generated
 */
@ProviderType
public class MyEntityLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.slemarchand.jsonws.sample.service.impl.MyEntityLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.slemarchand.jsonws.sample.model.UserInfos getUserInfos(
		long userId) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getUserInfos(userId);
	}

	public static com.slemarchand.jsonws.sample.model.UserInfos getUserInfos(
		long companyId, String screenName)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getUserInfos(companyId, screenName);
	}

	public static void updateUserInfos(long userId,
		com.slemarchand.jsonws.sample.model.UserInfosUpdate userInfosUpdate)
		throws com.liferay.portal.kernel.exception.PortalException {
		getService().updateUserInfos(userId, userInfosUpdate);
	}

	public static MyEntityLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<MyEntityLocalService, MyEntityLocalService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(MyEntityLocalService.class);

		ServiceTracker<MyEntityLocalService, MyEntityLocalService> serviceTracker =
			new ServiceTracker<MyEntityLocalService, MyEntityLocalService>(bundle.getBundleContext(),
				MyEntityLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}