/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.slemarchand.jsonws.sample.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MyEntityService}.
 *
 * @author Brian Wing Shun Chan
 * @see MyEntityService
 * @generated
 */
@ProviderType
public class MyEntityServiceWrapper implements MyEntityService,
	ServiceWrapper<MyEntityService> {
	public MyEntityServiceWrapper(MyEntityService myEntityService) {
		_myEntityService = myEntityService;
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public String getOSGiServiceIdentifier() {
		return _myEntityService.getOSGiServiceIdentifier();
	}

	@Override
	public com.slemarchand.jsonws.sample.model.UserInfos getUserInfos()
		throws com.liferay.portal.kernel.exception.PortalException {
		return _myEntityService.getUserInfos();
	}

	@Override
	public com.slemarchand.jsonws.sample.model.UserInfos getUserInfos(
		String screenName)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _myEntityService.getUserInfos(screenName);
	}

	@Override
	public void updateUserInfos(long userId,
		com.slemarchand.jsonws.sample.model.UserInfosUpdate userInfosUpdate)
		throws com.liferay.portal.kernel.exception.PortalException {
		_myEntityService.updateUserInfos(userId, userInfosUpdate);
	}

	@Override
	public MyEntityService getWrappedService() {
		return _myEntityService;
	}

	@Override
	public void setWrappedService(MyEntityService myEntityService) {
		_myEntityService = myEntityService;
	}

	private MyEntityService _myEntityService;
}