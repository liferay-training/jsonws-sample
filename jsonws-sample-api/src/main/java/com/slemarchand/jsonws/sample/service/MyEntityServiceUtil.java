/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.slemarchand.jsonws.sample.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for MyEntity. This utility wraps
 * {@link com.slemarchand.jsonws.sample.service.impl.MyEntityServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see MyEntityService
 * @see com.slemarchand.jsonws.sample.service.base.MyEntityServiceBaseImpl
 * @see com.slemarchand.jsonws.sample.service.impl.MyEntityServiceImpl
 * @generated
 */
@ProviderType
public class MyEntityServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.slemarchand.jsonws.sample.service.impl.MyEntityServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.slemarchand.jsonws.sample.model.UserInfos getUserInfos()
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getUserInfos();
	}

	public static com.slemarchand.jsonws.sample.model.UserInfos getUserInfos(
		String screenName)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getUserInfos(screenName);
	}

	public static void updateUserInfos(long userId,
		com.slemarchand.jsonws.sample.model.UserInfosUpdate userInfosUpdate)
		throws com.liferay.portal.kernel.exception.PortalException {
		getService().updateUserInfos(userId, userInfosUpdate);
	}

	public static MyEntityService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<MyEntityService, MyEntityService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(MyEntityService.class);

		ServiceTracker<MyEntityService, MyEntityService> serviceTracker = new ServiceTracker<MyEntityService, MyEntityService>(bundle.getBundleContext(),
				MyEntityService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}