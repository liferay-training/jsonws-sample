# JSON Web Service sample

Pour tester les JSON Web Services du sample : 

* http://localhost:8080/api/jsonws
* Choisir *mynamespace* dans la dropdown "Context Name"

Dans le cas d'une m�thode prenant en param�tre un objet complexe, l'interface utilisateur ne fonctionne pas. Pour tester il faut directement r�aliser les appels en Javascript. Exemple : 

```
Liferay.Service('/mynamespace.myentity/update-user-infos', {
    userId: 20139,
    userInfosUpdate: {
        firstName: "Life",
        lastName: "Ray"
    }
}, function(obj) {
    console.log(obj);
});
```


